==============================
Parts, operations and assembly
==============================

.. |Vrms| replace:: V\ :sub:`rms`
.. |mVrms| replace:: mV\ :sub:`rms`


.. contents::
   :depth: 2

Comments on the circuit diagram
===============================

In the following, please refer to the `circuit diagram`_ figure for
the various components and their locations.


Cable testing
-------------

The cable testing part is straight forward. One end of the cable is
connected to one of the output jacks on the left side of the device,
and the other end to one of the input jacks on the right side. Turning
the knob ``S1`` to different positions will send a current through the
separate wires in the cable. If the connection for the selected wire
is closed, i.e. there is no loose connection, respective diods ``D1,
D3-D10`` and ``D2, D14-D21`` will light up. If the attached cable is
suspect of having loose wires try bending the cable a bit and see if
the diod flickers.

The switch ``S3`` is used to select the cable type for the TRS/TS
jacks. Using the TRS configuration with a TS cable will indicate a
false short circuit between the T and the R, since R does not exist on
such a cable.

``D24`` and ``R23`` are components adjusts the voltage and current to
make the diods shine with approximately the same intensity.

``D25`` protects the battery from phantom power currents if the device
is plugged into a mixer with phantom power enabled, and the ``S1``
knob in a position where the phantom power would drive current through
the battery.


Switch transistors
------------------

Switch transistors ``T2-T6`` isolates the tone generator and phantom
power detection circuitry from signals when the device is used for
cable testing through the roundtrip output-to-input jacks. Without
these transistors, signals would go through the circuits and give rise
to false indications.


Continuity testing
------------------

Continuity testing is done by connecting e.g. ordinary multimeter
probes to the banana jacks marked ``CONT`` (``C`` in the
diagram). Diod ``D11`` provides visual indication of continuity, and
buzzer ``B1`` will give an audible indication. The buzzer may be
turned off by removing jumper ``J1``.


Virtual ground
--------------

``IC1`` is a virtual ground circuit that splits the power supply (9V)
into the range -4.5V-4.5V. ``D22`` is just a "power on" indicator,
showing that the tone generator circuitry is in operation.


Tone generator
--------------

The tone generator provides a balanced signal on the XLR and TRS
output jacks.

The OP-amplifier ``IC2/4`` together with the components surrounding it
make up a wein oscillator [#wein1]_ [#wein2]_ which generates a sine
wave at approximately 1 kHz, and ca 2 |Vrms| amplitude at output
pin 8. ``T1`` is operating in the ohmic region and functions as an
automatic gain control. The amplitude can be adjusted by adjusting
``R8``. However, keep in mind that the OP-amp used (LM324) is not rail
to rail, and the power supply is split to the range -4.5V to
4.5V. Hence, a too large amplitude will saturate the OP-amp.

``S2`` together with ``R10-R13`` forms a level selector between mic
(~6 |mVrms|), consumer equipment line level (~0.3 |Vrms|), and
professional equipment line level (~1.23 |Vrms|).

``IC2/3`` inverts the signal, this is needed in order to provide a
balanced signal on the XLR/TRS outputs. ``IC2/1`` and ``IC2/2`` are
buffers. These are needed since the output from ``IC2/4`` is not
enough to drive a line in signal.


.. [#wein1] https://myelectrons.com/wien-bridge-oscillator-low-thd/
.. [#wein2] https://learnabout-electronics.org/Oscillators/osc34.php

           
Phantom power detection
-----------------------

The phantom power indicators will light up as soon as a phantom power
greater than ca 4.5V is detected.

The op-amps ``IC3/A`` and ``IC3/B`` (an LM358) together with
``R17-R20`` form two simple comparators that make up the phantom power
detection. If the device is connected to a mixer e.g. through an
installed microphone cable and the tone generator is active
(preferably at the mic level) ``D12`` (hot) and ``D13`` (cold) will
light up if the mixer provides phantom power on the hot/cold wires.

``C4`` and ``C5`` together with ``D26-D29`` isolates and protects the
buffer op-amps from phantom power. Phantom power can be up to 52V
(48+/-4V), without the diodes the op-amps could get overloaded during
transients.


Part list
=========

Casing
------

====== ================== =================
Pieces Part               Suggestion
------ ------------------ -----------------
1      Hammond 1590B3BK   `Hammond 1590B3BK <https://www.hammfg.com/electronics/small-case/diecast/1590>`_
====== ================== =================

Connectors
----------

====== ================== =================
Pieces Part               Suggestion
------ ------------------ -----------------
1      XLR male           `Neutrik NC3MAH-0 <https://www.mouser.se/ProductDetail/neutrik/nc3mah-0/?qs=JfNPhaIww3KBJDY6RIiscw==>`_ 
1      XLR female         `Neutrik NC3FAH2-0 <https://www.mouser.se/ProductDetail/neutrik/nc3fah2-0/?qs=erV61lVMmP9ChIzp4pcKOA==>`_
1      Red/black RCA      `CUI Devices RCJ-2121 <https://www.mouser.se/ProductDetail/cui-devices/rcj-2121/?qs=WyjlAZoYn50FEl9sszhkhA==>`_
1      Black/red RCA      `CUI Devices RCJ-2112 <https://www.mouser.se/ProductDetail/cui-devices/rcj-2112/?qs=E%2FuzQwxm92dwNj9O3UKnBw==>`_
2      6.35 mm TRS        `Amphenol ACJS-MHDR <https://www.mouser.se/ProductDetail/amphenol/acjs-mhdr/?qs=NlNVDDZd7xTCfXBCJpjpjw==>`_
2      3.5 mm TRS         `3.5 mm noname <https://www.electrokit.com/produkt/3-5mm-jack-3-pol-chassi/>`_
2      8P8C Modular       `Amphenol RJE08-188-0110 <https://www.mouser.se/ProductDetail/amphenol/rje08-188-0110/?qs=QsjYS2XzMokkCtQpIsfDTQ==>`_
1      Red banana jack    `Pomona 5931-2 <https://www.mouser.se/ProductDetail/pomona/5931-2/?qs=%2FALxmKEvhOJfLOu9fbx7Sg==>`_
1      Black banana jack  `Pomona 5931-0 <https://www.mouser.se/ProductDetail/pomona/5931-0/?qs=%2FALxmKEvhOK1rNcTZZMdxw==>`_
====== ================== =================

Switches
--------

====== ===================== =================
Pieces Part                  Suggestion
------ --------------------- -----------------
1      12 pole switch (`S1`) `NKK Switches MRK112-BA <https://www.mouser.se/ProductDetail/nkk-switches/mrk112-ba/?qs=uKp9oNAyH9Yi5JbEyoVNOg==>`_
1      SP3T (`S2`)           `Knitter-Switch MFP 130 <https://www.knitter-switch.com/eng/Series/70>`_
1      DPDT (`S3`)           `DPDT noname <https://www.electrokit.com/produkt/skjutomkopplare-2-pol-on-on-panel-pcb/>`_
                             (or `Knitter-Switch MFS-201-N <https://www.electrokit.com/produkt/skjutomkopplare-2-pol-on-on-panel-pcb/>`_ NOTE: Needs some layout adjustments)
====== ===================== =================


Circuit elements
----------------

====== ===========
Pieces Resistors
------ -----------
1      107
1      750
2      1k
2      2.2k
1      4.42
2      4.7k
1      6.19k
1      8.2k
6      10k
1      12k
1      15k
1      68k
2      158k
2      150k
1      1M
1      3.9M
====== ===========

====== ===========
Pieces Capacitors
------ -----------
2      1nF
1      0.47uF
2      10uF
====== ===========

====== ==================
Pieces Semiconductors
------ ------------------
3      1N4148
1      2N3819
4      BZX55C3V9
5      BS170
====== ==================

====== =========================
Pieces IC
------ -------------------------
1      TLE2426 (virtual ground)
1      LM324AN
1      LM358AN
====== =========================

====== ====================== ==================
Pieces Low power LEDS         Suggestion              
------ ---------------------- ------------------
2      3 mm green (2.65V 2mA) `Kingbright WP710A10LZGCK <https://www.mouser.se/ProductDetail/kingbright/wp710a10lzgck/?qs=6oMev5NRZMGyFPl4gWV9yg==>`_
11     3 mm orange (1.8V 2mA) `Kingbright WP710A10LSECK/J4 <https://www.mouser.se/ProductDetail/kingbright/wp710a10lseck-j4/?qs=6oMev5NRZMFDPx128U6aUg==>`_
9      3 mm blue (2.65V 2mA)  `Kingbright WP710A10LVBC/D <https://www.mouser.se/ProductDetail/kingbright/wp710a10lvbc-d/?qs=6oMev5NRZMF7xED66hMqMg==>`_
====== ====================== ==================

.. note:: Although the LM324 and LM358 might not be the the best
          circuits regarding distortion (e.g. they suffer from
          cross-over distortion) they do allow for the input voltage
          to be higher than the supply voltage, which is very handy in
          phantom power detection.

Fastening elements
------------------

====== ===========
Pieces Nuts
------ -----------
6      M2.5
4      M2
====== ===========

====== =======================
Pieces Skrews
------ -----------------------
4        M2 x 6
4      M2.5 x 5 distance
6      M2.5 x 6
6      M2.5 x 10 self tapping
====== =======================

====== =================================
Pieces Washers
------ ---------------------------------
8      M2.5 x 0.5
1      Essentra MNI 4-5 shoulder washer
====== =================================



Assembly
========

Central to assembly and construction is the layout_ drawing. It was
done in `Inkscape <https://inkscape.org/>` but can possibly be opened
in other programs for svg editing as well. It is a multilayer
blueprint of all of the different elements of the design. It includes
schematic boxes for the connectors just to make sure everything fits
together.

The overall order of assembly employed:

#. Engrave the casing. (`1 <pics/box-engraved-front.jpg>`_,
   `2 <pics/box-engraved-back.jpg>`_)
#. Drill and cut out all the holes in the casing. (`3 <pics/box-holes.jpg>`_)
#. Fasten the distance screws for PCB mounting in the casing.
#. Cut out and fold the mountings_.
#. Cut out the circuit board. Make sure the hole matrix in the circuit
   board lines up with the holes in the casing. (`4 <pics/circuit-board-cutting.jpg>`_,
   `5 <pics/internals.jpg>`_)
#. Solder components to circuit board. (`6 <pics/circuit-board-with-comp.jpg>`_)
#. Prepare wires with CRIMP terminals.
#. Prepare wire groups with shrinking tubes.
#. Solder wire groups to 8P8C connectors.
#. Solder RCA jacks to wires.
#. Mount RCA jacks to casing.
#. Mount circuit board in casing, and fasten the 12-pole switch
   (``S1``) with nut.
#. Solder wires to the DPDT switch (``S3``)
#. Solder wires to the 6.35 mm TRS jacks.
#. Mount 6.35 mm TRS jacks in the casing.
#. Solder 3.5 mm TRS jacks onto the wires from the 6.35 mm TRS jacks.
#. Mount 3.5 mm TRS jacks.
#. Mount 8P8C connectors.
#. Solder wires onto banana jacks.
#. Mound banana jacks.
#. Mount XLR connectors.
#. Solder wires from TRS, 8P8C and DPDT (``S3``) switch onto the XLR
   connectors. (`7 <pics/internals-final.jpg>`_)
   


Insulated connectors
--------------------

To be able to test shield/ground wiring of certain cables all
connectors where chosen so that they are insulated from the
casing. However, in order to achieve this for the female XLR connector
it needed a slight modification. The separate ground pin was removed,
1.5 mm of it was cut off, and then it was reinserted again. (Be
careful not to cut off too much, it will be hard to reinsert it then.)
Also, the screw that usually connects the ground pin with the casing
needed to be fastened through a nylon shoulder washer.


Casing
------

The case is a `1590B3BK
<https://www.hammfg.com/electronics/small-case/diecast/1590>`_ die-
cast aluminium box from Hammond.

The text on the casing was done by laser engraving. It is not really
engraved in the metal, the laser just evaporated the black powder
coating, and thus uncovered the metal beneath. See pictures `1
<pics/box-engraved-front.jpg>`_ and `2 <pics/box-engraved-back.jpg>`_

Holes for the different connectors may of course vary, but with the
ones listed the following drills where used

* 2.0 mm for 8P8C connector mounting
* 2.5 mm for battery and PCB mountings
* 3.0 mm for the diods and XLR mountings
* 3.5 mm for the bottom XLR female connector mounting
* 5.5 mm for the 12 pole switch (``S1``)
* 8.0 mm for 3.5 mm TRS jacks
* 10.0 mm for RCA connector holes
* 11,5 mm for 6.0 mm TRS jacks
* 12.0 mm for banana connectors

The rectangular holes for the 8P8C jack where done with metal saw, 2
mm drill and a file. Likewise the rectangular holes for the switches
``S2`` and ``S3`` where done with 2 mm drill and a set of small metal
files.

The 22 mm holes for the XLR connectors where done by drilling many
small holes close to each other along the perimeter with a 2 mm drill,
and then drilling away the material in the middle with a larger
drill. Afterwards the perimeter was smoothed with half round file with
a suitable diamater. The result can be seen in `picture 3
<pics/box-holes.jpg>`_.


Mountings
---------

The mountings_ file mountings.svg contains blueprints for the
mountings used for fastening the 8P8C connectors and the battery.

The blueprints can be cut out, glued to a pice of plate and cut
out. For this project, a piece of 1 mm galvanized steel plate was
used. Each line in the drawings mark a fold of the piece. Please refer
to the layout file in order to figure out in which direction to fold
at each line.


Circuit board
-------------

For this project, an experiment card with 2.54 x 2.54 mm matrix of
holes where used, where the holes are connected pairwise. The circuit
board shape was cut out according to the shape in the layout_ file.

.. attention:: The circuit board in the layout_ .svg file is drawn as
               seen from the top. The "circuit board top" layer is
               facing the top of the casing, and the "circuit board
               bottom" is facing the bottom of the casing. Since it is
               drawn seen from the top, the bottom layer needs to be
               mirrored in order to see it from the bottom view.
               
The circuit board "top layer" is the copper plated layer. Most of the
soldering is straight forward. Some of the resistors are soldered
"standing" in order to save space.

Worth to mention is the soldering of the diods. To get the position
and distance correct between the PCB and casing, the diods where put
into the hole matrix in the PCB at their correct positions. Then the
PCB was inserted into the casing as in `picture 5
<pics/internals.jpg>`_.  The case was held upside down while making
sure all the diods fell into the corresponding holes in the casing,
and then a small drop of super-glue was put in each hole having a pin
from a diode. (This is on the non-plated side of the PCB.) After the
glue had dried the diodes where soldered in place on the plated side.


Connector wiring
----------------

The connectors are more or less daisy chained together in the order
``RCA -> 8P8C -> XLR -> (DPDT) -> TRS (6.35 mm) -> TRS (3.5 mm)``.

The 8P8C connector acts as the connection point to the PCB. The
connections between the board and connectors are done by crimp
terminals (wrapped in shrinking tube) on the wires from the connectors
to pins on the board. Each pin is marked by a small red dot with a tap
on it and is located in the "circuit board bottom pins" layer in the
layout drawing.

.. tip:: The space at the back of the 8P8C connector is fairly limited
         but is the meeting point for the wires. The soldering is
         simplified by creating wire groups with 3 wires (6 groups are
         needed) and 2 wires (2 groups are needed). Skin about 15 mm
         of all the wires in the group, twine the wires together and
         put a 10 mm piece of shrinking tube around them. See the
         `wire groups figure <wire-groups.svg>`_.


.. _layout: layout.svg
.. _mountings: mountings.svg
.. _`circuit diagram`: circuit-diagram.svg
