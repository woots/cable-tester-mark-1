=== ===========
No   Resistors
--- -----------
1    107
1    750
2    2.2k
1    4.42
2    4.7k
1    6.19k
1    8.2k
6    10k
1    12k
1    15k
1    53.6k (68k)
2    158k
2    150k
1    7.5M (3.5M)
=== ===========

=== ===========
No   Capacitors
--- -----------
2    1nF
1    0.47uF
2    10uF
=== ===========

=== ==================
No   Semiconductors
--- ------------------
2    1N4148
1    2N3819
2    WP710A10LZGCK
11   WP710A10LSECK/J4
9    WP710A10LVBC/D
=== ==================

=== ===========
No   Nuts
--- -----------
6    M2.5
6    M2
=== ===========

=== =======================
No   Skrews
--- -----------------------
2      M2 x 10
2      M2 x 6
4    M2.5 x 5 distance
6    M2.5 x 6
6    M2.5 x 10 self tapping
=== ========================

=== =================================
No   Washers
--- ---------------------------------
8    M2.5 x 0.5
1    Essentra MNI 4-5 shoulder washer
=== =================================




Parts
=====

565-5931-0 5931-0 2.94 Pomona Electronics Banana jack BLK
565-5931-2 5931-2 2.94 Pomona Electronics Banana jack RED

(568-NC3FD-L-1-B NC3FD-L-B-1 4.08 Neutrik XLR Female)
568-NC3FD-L-1-BAG-0 NC3FDLBAG-0-1 3.59 Neutrik XLR Female no latch
568-NC3MD-L-BAG-1 NC3MD-L-BAG-1 3.49 Neutrik XLR Male

(568-NE8FDV-B NE8FDV-B 5.58 Neutrik RJ45 Female)
(568-NE8FDV-B NE8FDV-B 5.58 Neutrik RJ45 Female)
(523-RJE082880119 RJE082880119)

523-RJE081880110 RJE08-188-0110 10.82 kr Amphenol RJ45 Female
523-RJE081880110 RJE08-188-0110 10.82 kr Amphenol RJ45 Female

(568-NYS212  NYS212 0.798 REAN Telefonkontakter 1/4")
(568-NYS212  NYS212 0.798 REAN Telefonkontakter 1/4")
https://www.elfa.se/en/jack-socket-panel-mount-35-mm-black-poles-amphenol-entertainment-acjs-mhdr/p/14225571?q=*&pos=22&origPos=177&origPageSize=50&track=true
523-ACJS-MHDR ACJS-MHDR 13.70 kr Amphenol Telefonkontakter 1/4 x 2

41017350 29 kr 3.5mm telefonkontakt https://www.electrokit.com/produkt/3-5mm-jack-3-pol-chassi/
41017350 29 kr 3.5mm telefonkontakt https://www.electrokit.com/produkt/3-5mm-jack-3-pol-chassi/

490-RCJ-2123 RCJ-2123 1.50 CUI RCA-kontakter.
490-RCJ-2123 RCJ-2123 1.50 CUI RCA-kontakter.


(122-BX0023 BX0023 6.45 Bulgin 9V batterispännen)
534-079 79 0.428 Keystone Electronics 9V batterispänne

604-WP710A10LSECKJ3 WP710A10LSECK/J3 0.531 3mm Röd lysdiod
604-WP710A10LSECKJ4 WP710A10LSECK/J4 0.317 3mm Orange lysdiod x 10
604-WP710A10LVBCD WP710A10LVBC/D 0.292 3mm Blå lysdiod x 10
604-WP710A10LZGCK WP710A10LZGCK 0.504 3mm Grön lysdiod

633-MRK112-BA MRK112-BA 11.90 NKK Switches 1 pol 12-läges omkopplare

41003373 6 kr Skjutomkopplare 1-pol https://www.electrokit.com/produkt/skjutomkopplare-1-pol-on-on-lodoron/

(633-MS20AFW01 MS20AFW01 70.47 kr SP3T Glidomkopplare)
611-OS103011MS8QP1 OS103011MS8QP1 4,82 kr C&K SP3T Glidomkopplare

2*2.94+3.59+3.49+5.58+5.58+2*0.798+2*1.50+0.428+2*0.531+13*0.317+13*0.292+2*0.504+11.90 = 50


Wave generator
==============

op amp
------
https://www.eeeguide.com/oscillator-amplitude-stabilization-circuit/ (main design)
https://myelectrons.com/wien-bridge-oscillator-low-thd/ (main design)
http://www.learnabout-electronics.org/Oscillators/osc34.php (cross-over distorsion)
http://www.zen22142.zen.co.uk/Design/wien_osc/Wien-Bridge%20Oscillator.htm (frequency control)


transistor
----------
http://learningaboutelectronics.com/Articles/Sine-wave-generator-circuit-with-a-transistor.php
https://circuitdigest.com/electronic-circuits/sine-wave-generator-circuit/
https://www.electronics-tutorials.ws/oscillator/rc_oscillator.html

https://www.electronics-tutorials.ws/oscillator/wien_bridge.html



