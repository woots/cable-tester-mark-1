===================
Cable Tester Mark I
===================

.. image:: pics/title.jpg
   :align: center
   :width: 100%

.. |Vrms| replace:: V\ :sub:`rms`
.. |mVrms| replace:: mV\ :sub:`rms`


Overview
========

This is a DIY cable tester project. The built device is used to test
cables, mainly used in audio applications, for loose connections. It
includes a tone generator that enables testing of installed
connections e.g. to/from a mixer or speaker. It also includes phantom
power detection to see if a mixer outputs phantom power or not over
the connected wire.


Features
========

* Rugged casing

* Small form factor, 116x77x55 mm

* Testing of cables with connectors in any combination of

  * XLR (3-pin)
  * RCA
  * TRS/TS (6.35 mm, 3.5 mm)
  * 8P8C Modular connector (sometimes referred to as "RJ45")

* Continuity testing with crocodile clips or ordinary multimeter
  probes.

* Phantom power detection

* Sine wave generator at 3 different signal levels

  * mic (ca -45 dBu, or ~5 |mVrms|)
  * consumer line level (ca -9.5 dbV, or ~0.26 |Vrms|)
  * professional line level (ca +4 dBu, or 1.23 |Vrms|)

* 9 V battery power supply (`PP3 <https://en.wikipedia.org/wiki/Nine-volt_battery>`)


Assembly
========

Hopefully most of the pieces fall naturally into place. However, there
are a few things that need some extra care. Please see the separate
`document on device assembly`_ for those points.

.. _`document on device assembly`: assembly.rst


License
=======

.. |ccLicenseImg| image:: https://i.creativecommons.org/l/by-sa/4.0/88x31.png
   :alt: Creative Commons License
   :align: middle
   
|ccLicenseImg| This work is licensed under a
`Creative Commons Attribution-ShareAlike 4.0 International License`__.

.. _ccLicense: http://creativecommons.org/licenses/by-sa/4.0/

__ ccLicense_

